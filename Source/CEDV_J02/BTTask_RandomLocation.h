#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_RandomLocation.generated.h"

/**
 * Insert in the blackboard a random location reacheble for the character
 */
UCLASS()
class CEDV_J02_API UBTTask_RandomLocation : public UBTTaskNode
{
	GENERATED_BODY()

public:
	/** The Variable to store the random location */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Blackboard)
	FBlackboardKeySelector BlackboardKey;
	
protected:
	/** Function called when the behaviour tree invoke this task */
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};

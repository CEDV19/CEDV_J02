#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "WeaponShoot.h"
#include "DamagedObject.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UDamagedObject : public UInterface
{
	GENERATED_BODY()
};

/**
 *	This interface must be implemented by the actors which want be targeteable for the WeaponShoot 
 */
class CEDV_J02_API IDamagedObject
{
	GENERATED_BODY()

public:
	/** This method is executed when the actor is collisioning a WeaponShoot */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category=DamagedObject)
		void ObjectDamaged(AWeaponShoot* ObjectDamaging);
};

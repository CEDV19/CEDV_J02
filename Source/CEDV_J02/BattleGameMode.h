#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "BattleGameMode.generated.h"

/**
 * The Game Mode of the Battle level
 */
UCLASS()
class CEDV_J02_API ABattleGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	ABattleGameMode();
};

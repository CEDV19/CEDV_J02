#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WeaponShoot.generated.h"

/**
 *	The abstract bullet wich is fired by the character. Also has static methods to initialize its specializations
 */
UCLASS()
class CEDV_J02_API AWeaponShoot : public AActor
{
	GENERATED_BODY()
	
public:	
	AWeaponShoot();

	/** Collider of the WeaponShoot */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Weapon)
		int Damage;

	/** Spawn the Weapon Shoot and implements its behaviour */
	UFUNCTION(BlueprintCallable, Category=WeaponShoot)
		virtual void Shoot();

	/** Return the Static Mesh of the Weapon Shoot */
	UFUNCTION(BlueprintCallable, Category = WeaponShoot)
		UStaticMeshComponent* GetMesh() const;

protected:
	/** The Scene component to set the mesh and collider */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Collider, meta = (AllowPrivateAccess = "true"))
		class USceneComponent* Scene;

	/** Static Mesh of the WeaponShoot */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Mesh, meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* Mesh;

	/** Collider of the WeaponShoot */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Scene, meta = (AllowPrivateAccess = "true"))
		class USphereComponent* Collider;

	/** Auxiliary Object damaged variable */
	class IDamagedObject* TemporalObjectDamaged;
	
	/** This method is called when the Weapon Shoot hit another object */
	UFUNCTION(BlueprintCallable, Category = WeaponShoot)
		virtual void OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	//////////////////////////////////////////////////////////////////////////
	// Weapon Factory
public:
	static AWeaponShoot* CreateAndSpawnKA47Shoot(UWorld* World, FVector const& Location, FRotator const& Rotation, const FActorSpawnParameters& SpawnParameters = FActorSpawnParameters());
};

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "BattleAIController.generated.h"

/**
 * This class posses the ThirPersonCharacter and starts and stops the behavior tree
 */
UCLASS()
class CEDV_J02_API ABattleAIController : public AAIController
{
	GENERATED_BODY()
	
public:
	ABattleAIController();

	/** Start Behaviour Tree */
	UFUNCTION(BlueprintCallable, Category=ThirdPersonCharacter)
		void StartBehaviorTree();

	/** Stop Behaviour Tree */
	UFUNCTION(BlueprintCallable, Category=ThirdPersonCharacter)
		void StopBehaviorTree();

protected:
	/** The Easy IA Behaviour Tree */
	UPROPERTY()
	class UBehaviorTree* EasyBehaviorTree;

	/** Caller when the Character is possed */
	virtual void OnPossess(APawn* aPawn) override;
};

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "BattleGameInstance.generated.h"

/**
 * Stores and loads the records of the game and important variables of the main cycle of the game.
 */
UCLASS()
class CEDV_J02_API UBattleGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	UBattleGameInstance();

	/** The recors variables */
	UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		TArray<FString> records;

	UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		FString recordName1;

	UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		FString recordName3;

	UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		FString recordName2;

	UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		FString recordScore1;

	UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		FString recordScore2;

	UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		FString recordScore3;

	/** The current Score of the Game */
	UPROPERTY(Category = ClassVariables, EditAnywhere, BlueprintReadWrite)
		int CurrentScore;

	UFUNCTION(BlueprintCallable)
		TArray<FString> LoadBestRecords();

	UFUNCTION(BlueprintCallable)
		void SaveBestRecords();

	UFUNCTION(BlueprintCallable)
		void SetNewRecord(FString name, int score);

	UFUNCTION(BlueprintCallable)
		void CheckIniFile();

protected:
	/** Config file Keys */
	FString configRecordsSection;
	FString recordName1Key;
	FString recordName3Key;
	FString recordName2Key;
	FString recordScore1Key;
	FString recordScore2Key;
	FString recordScore3Key;
};

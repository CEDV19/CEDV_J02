#include "BattleGameInstance.h"
#include "ConfigCacheIni.h"

UBattleGameInstance::UBattleGameInstance() : UGameInstance()
{
	// Initialization
	configRecordsSection = "Records";
	recordName1Key = "RecordName1";
	recordName2Key = "RecordName2";
	recordName3Key = "RecordName3";
	recordScore1Key = "RecordScore1";
	recordScore2Key = "RecordScore2";
	recordScore3Key = "RecordScore3";
	recordName1 = "EMPTY";
	recordName2 = "EMPTY";
	recordName3 = "EMPTY";
	recordScore1 = "100";
	recordScore2 = "100";
	recordScore3 = "100";
	CurrentScore = 1000;
}

TArray<FString> UBattleGameInstance::LoadBestRecords()
{
	CheckIniFile();
	GConfig->Flush(true, GGameIni);
	GConfig->GetString(*configRecordsSection, *recordName1Key, recordName1, GGameIni);
	GConfig->GetString(*configRecordsSection, *recordName2Key, recordName2, GGameIni);
	GConfig->GetString(*configRecordsSection, *recordName3Key, recordName3, GGameIni);
	GConfig->GetString(*configRecordsSection, *recordScore1Key, recordScore1, GGameIni);
	GConfig->GetString(*configRecordsSection, *recordScore2Key, recordScore2, GGameIni);
	GConfig->GetString(*configRecordsSection, *recordScore3Key, recordScore3, GGameIni);
	return records;
}

void UBattleGameInstance::SaveBestRecords()
{
	GConfig->SetString(*configRecordsSection, *recordName1Key, *recordName1, GGameIni);
	GConfig->SetString(*configRecordsSection, *recordName2Key, *recordName2, GGameIni);
	GConfig->SetString(*configRecordsSection, *recordName3Key, *recordName3, GGameIni);
	GConfig->SetString(*configRecordsSection, *recordScore1Key, *recordScore1, GGameIni);
	GConfig->SetString(*configRecordsSection, *recordScore2Key, *recordScore2, GGameIni);
	GConfig->SetString(*configRecordsSection, *recordScore3Key, *recordScore3, GGameIni);
	GConfig->Flush(false, GGameIni);
}

void UBattleGameInstance::SetNewRecord(FString name, int score)
{
	LoadBestRecords();
	if (score < FCString::Atoi(*recordScore1))
	{
		recordName3 = recordName2;
		recordScore3 = recordScore2;
		recordName2 = recordName1;
		recordScore2 = recordScore1;
		recordName1 = name;
		recordScore1 = FString::FromInt(score);
	}
	else if (score < FCString::Atoi(*recordScore2))
	{
		recordName3 = recordName2;
		recordScore3 = recordScore2;
		recordName2 = name;
		recordScore2 = FString::FromInt(score);
	}
	else if (score < FCString::Atoi(*recordScore3))
	{
		recordName3 = name;
		recordScore3 = FString::FromInt(score);
	}
	SaveBestRecords();
}

void UBattleGameInstance::CheckIniFile()
{
	// Config file
	FString configFilePath = FPaths::ProjectSavedDir();
	configFilePath += FString("Config/Windows/Game.ini");
	if (!FPaths::FileExists(configFilePath)) 
	{
		SaveBestRecords();
	}
}

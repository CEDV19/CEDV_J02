#include "KA47Shoot.h"
#include "Components/StaticMeshComponent.h"
#include "ConstructorHelpers.h"

AKA47Shoot::AKA47Shoot() : AWeaponShoot() 
{
	// Configure properties
	auto StaticMesh = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("Blueprint'/Game/Models/Weapons/KA47_Ammo'"));
	if (StaticMesh.Succeeded()) {
		Mesh->SetStaticMesh(StaticMesh.Object);
		Mesh->SetRelativeRotation(FRotator(0, 270, 0));
	}
	Damage = 50;
}


void AKA47Shoot::Shoot()
{
	Mesh->SetSimulatePhysics(true);
	Mesh->AddForceAtLocationLocal(FVector(0, 20000, 0), Mesh->GetComponentLocation());
}


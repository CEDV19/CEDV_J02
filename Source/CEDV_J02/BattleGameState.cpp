#include "BattleGameState.h"
#include "BattleAIController.h"
#include "BattleGameInstance.h"
#include "EngineUtils.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/TargetPoint.h"

ABattleGameState::ABattleGameState() : MaxTimeOfTurn(15), CurrentPlayer(0), CurrentIA(0)
{
	// Set default variables
	GameTurn = Turn::Player;
	TimeOfTurn = MaxTimeOfTurn;
	Score = 0;

	// Get necessaries Blueprint Classes
	auto PlayerTarget = ConstructorHelpers::FClassFinder<ATargetPoint>(TEXT("TargetPoint'/Game/Blueprints/TargetPlayer'"));
	if (PlayerTarget.Succeeded()) {
		PlayerTargetClass = PlayerTarget.Class;
	}

	auto EnemyTarget = ConstructorHelpers::FClassFinder<ATargetPoint>(TEXT("TargetPoint'/Game/Blueprints/TargetEnemy'"));
	if (EnemyTarget.Succeeded()) {
		EnemyTargetClass = EnemyTarget.Class;
	}

	auto PlayerCharacter = ConstructorHelpers::FClassFinder<AThirdPersonCharacter>(TEXT("ThirdPersonCharacter'/Game/Blueprints/ThirdPersonCharacterPlayer'"));
	if (PlayerCharacter.Succeeded()) {
		PlayerCharacterClass = PlayerCharacter.Class;
	}

	auto EnemyCharacter = ConstructorHelpers::FClassFinder<AThirdPersonCharacter>(TEXT("ThirdPersonCharacter'/Game/Blueprints/ThirdPersonCharacterEnemy'"));
	if (EnemyCharacter.Succeeded()) {
		EnemyCharacterClass = EnemyCharacter.Class;
	}
}

void ABattleGameState::FinishTurn()
{
	GameTurn = GameTurn == Turn::Player ? Turn::IA : Turn::Player;
	if (GameTurn == Turn::Player) 
	{
		if(CurrentIA == 0) 
		{
			Cast<ABattleAIController>(Enemies[Enemies.Num() - 1]->GetController())->StopBehaviorTree();
		}
		else
		{
			Cast<ABattleAIController>(Enemies[(CurrentIA - 1) % Enemies.Num()]->GetController())->StopBehaviorTree();
		}
		RemoveDeadCharacters();
		CheckFinishConditions();
		if (Players.Num() > 0)
		{
			Controller->UnPossess();
			Controller->Possess(Players[CurrentPlayer]);
			Players[CurrentPlayer]->SetState(CharacterState::Movement);
			CurrentPlayer = (CurrentPlayer + 1) % Players.Num();
		}
	}
	else
	{
		if(CurrentPlayer == 0) 
		{
			Players[Players.Num() - 1]->ResetMovements();
		}
		else
		{
			Players[(CurrentPlayer - 1) % Players.Num()]->ResetMovements();
		}
		RemoveDeadCharacters();
		CheckFinishConditions();
		if (Enemies.Num() > 0)
		{
			Controller->SetViewTarget(Enemies[CurrentIA]);
			Cast<ABattleAIController>(Enemies[CurrentIA]->GetController())->StartBehaviorTree();
			CurrentIA = (CurrentIA + 1) % Enemies.Num();
		}
	}
	TimeOfTurn = MaxTimeOfTurn;
	++Score;
}

void ABattleGameState::ProcessTurn()
{
	--TimeOfTurn;
	if (TimeOfTurn <= 0) {
		FinishTurn();
	}
}

void ABattleGameState::CheckFinishConditions()
{
	// If you lose
	if (Players.Num() == 0)
	{
		UGameplayStatics::OpenLevel(GetWorld(), "GameOver");
	}

	// If you win
	if (Enemies.Num() == 0)
	{
		Cast<UBattleGameInstance>(GetWorld()->GetGameInstance())->CurrentScore = Score;
		UGameplayStatics::OpenLevel(GetWorld(), "GameWin");
	}
}

void ABattleGameState::RemoveDeadCharacters()
{
	// Players
	TArray<AThirdPersonCharacter*> CheckPlayers = Players;
	for (AThirdPersonCharacter* Character : CheckPlayers)
	{
		if (Character->IsDead())
		{
			Players.Remove(Character);
		}
	}
	if (CurrentPlayer >= Players.Num())
	{
		CurrentPlayer = Players.Num() - 1;
	}

	// IA
	TArray<AThirdPersonCharacter*> CheckEnemies = Enemies;
	for (AThirdPersonCharacter* Character : CheckEnemies)
	{
		if (Character->IsDead())
		{
			Enemies.Remove(Character);
		}
	}
	if (CurrentIA >= Enemies.Num())
	{
		CurrentIA = Enemies.Num() - 1;
	}
}

void ABattleGameState::BeginPlay()
{
	Super::BeginPlay();

	// Get all target Points in the Map
	TArray<ATargetPoint*> PlayerTargets;
	TArray<ATargetPoint*> EnemyTargets;
	for (TActorIterator<ATargetPoint> TargetPointIterator(GetWorld()); TargetPointIterator; ++TargetPointIterator)
	{
		if (TargetPointIterator->IsA(PlayerTargetClass))
		{
			PlayerTargets.Add(*TargetPointIterator);
		}
		else if (TargetPointIterator->IsA(EnemyTargetClass)) 
		{
			EnemyTargets.Add(*TargetPointIterator);
		}
	}

	// Players creation
	for (int iterations = 0; iterations < PlayerTargets.Num(); ++iterations)
	{
		Players.Add(GetWorld()->SpawnActor<AThirdPersonCharacter>(PlayerCharacterClass, PlayerTargets[iterations]->GetActorLocation(), PlayerTargets[iterations]->GetActorRotation()));
		Players[iterations]->SetState(CharacterState::Movement);
	}
	for (int iterations = 0; iterations < EnemyTargets.Num(); ++iterations)
	{
		Enemies.Add(GetWorld()->SpawnActor<AThirdPersonCharacter>(EnemyCharacterClass, EnemyTargets[iterations]->GetActorLocation(), EnemyTargets[iterations]->GetActorRotation()));
		Enemies[iterations]->SetState(CharacterState::IA);
		Enemies[iterations]->SpawnDefaultController();
	}

	// Start Game
	Controller = GetWorld()->GetFirstPlayerController();
	Controller->Possess(Players[CurrentPlayer]);
	++CurrentPlayer;

	// Set The turns timer
	GetWorldTimerManager().SetTimer(TimerTurnHandle, this, &ABattleGameState::ProcessTurn, 1.0f, true, 1.0f);
}

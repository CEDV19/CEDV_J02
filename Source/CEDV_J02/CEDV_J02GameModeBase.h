#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CEDV_J02GameModeBase.generated.h"

/**
 * The game mode of the Battle level
 */
UCLASS()
class CEDV_J02_API ACEDV_J02GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	ACEDV_J02GameModeBase();
	
};

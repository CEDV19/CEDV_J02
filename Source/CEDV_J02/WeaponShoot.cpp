#include "WeaponShoot.h"
#include "DamagedObject.h"
#include "KA47Shoot.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"
#include "Components/SceneComponent.h"

// Sets default values
AWeaponShoot::AWeaponShoot() : AActor()
{
	// Create the root component
	Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	Scene->SetupAttachment(RootComponent);

	// Create the Mesh of the WeaponShoot
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Scene);

	// Create Sphere Collider of the shoot
	Collider = CreateDefaultSubobject<USphereComponent>(TEXT("Collider"));
	Collider->SetupAttachment(Mesh);
	Collider->RelativeLocation = FVector(0, 0, 0);
	Collider->SetSphereRadius(5);
	Collider->OnComponentBeginOverlap.AddDynamic(this, &AWeaponShoot::OnOverlap);

	// Default variables
	Damage = 5;
}

void AWeaponShoot::OnOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	TemporalObjectDamaged = Cast<IDamagedObject>(OtherActor);
	if (TemporalObjectDamaged != nullptr) {
		TemporalObjectDamaged->ObjectDamaged_Implementation(this);
		Destroy();
	}
}


void AWeaponShoot::Shoot() {}

UStaticMeshComponent* AWeaponShoot::GetMesh() const
{
	return Mesh;
}

//////////////////////////////////////////////////////////////////////////
// Weapon Factory

AWeaponShoot* AWeaponShoot::CreateAndSpawnKA47Shoot(UWorld* World, FVector const& Location, FRotator const& Rotation, const FActorSpawnParameters& SpawnParameters)
{
	return World->SpawnActor<AWeaponShoot>(AKA47Shoot::StaticClass(), Location, Rotation, SpawnParameters);
}
#include "BattleAIController.h"
#include "ThirdPersonCharacter.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BlackboardData.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "ConstructorHelpers.h"

ABattleAIController::ABattleAIController() : Super()
{
	// Set the difficulties behaviour tree
	auto EasyBehabiourTreeObject = ConstructorHelpers::FObjectFinder<UBehaviorTree>(TEXT("BehaviourTree'/Game/IA/IABehaviorEasy'"));
	if(EasyBehabiourTreeObject.Succeeded())
	{
		EasyBehaviorTree = EasyBehabiourTreeObject.Object;
	}
}

void ABattleAIController::StartBehaviorTree()
{
	GetBlackboardComponent()->SetValueAsBool("ExecuteBehaviorTree", true);
}

void ABattleAIController::StopBehaviorTree()
{
	GetBlackboardComponent()->SetValueAsBool("ExecuteBehaviorTree", false);
}

void ABattleAIController::OnPossess(APawn* aPawn)
{
	Super::OnPossess(aPawn);
	SetOwner(aPawn);
	AThirdPersonCharacter* ThirdPersonCharacter = Cast<AThirdPersonCharacter>(aPawn);
	if(ThirdPersonCharacter->GetState() == CharacterState::IA) 
	{
		RunBehaviorTree(EasyBehaviorTree);
	}
}

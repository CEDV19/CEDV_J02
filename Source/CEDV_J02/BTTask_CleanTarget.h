#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "BTTask_CleanTarget.generated.h"

/**
 * Clean the Character target
 */
UCLASS()
class CEDV_J02_API UBTTask_CleanTarget : public UBTTaskNode
{
	GENERATED_BODY()
	
protected:
	/** Function called when the behaviour tree invoke this task */
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};

#pragma once

#include "CoreMinimal.h"
#include "WeaponShoot.h"
#include "KA47Shoot.generated.h"

/**
 * A spetialized bullet wich is fired by the character.
 */
UCLASS()
class CEDV_J02_API AKA47Shoot : public AWeaponShoot
{
	GENERATED_BODY()
	
public:
	AKA47Shoot();

	/** Spawn the Weapon Shoot and implements its behaviour */
	virtual void Shoot() override;
};

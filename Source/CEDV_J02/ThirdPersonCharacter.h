#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "DamagedObject.h"
#include "ThirdPersonCharacter.generated.h"

/** The State of the ThirdPersonCharacter */
UENUM(BlueprintType)
enum class CharacterState : uint8
{
	Movement	UMETA(DisplayName="Movement"),
	AimShoot	UMETA(DisplayName="AimShoot"),
	IA			UMETA(DisplayName="IA")
};

/**
 *	The main character of the Battle level
 */
UCLASS()
class CEDV_J02_API AThirdPersonCharacter : public ACharacter, public IDamagedObject
{
	GENERATED_BODY()

public:
	AThirdPersonCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	/** Get the state of the character */
	UFUNCTION(BlueprintCallable, Category=ThirdPersonCharacter)
		CharacterState GetState() const;

	/** Set the state of the character */
	UFUNCTION(BlueprintCallable, Category=ThirdPersonCharacter)
		void SetState(CharacterState NewState);

	/** Check if the character is moving */
	UFUNCTION(BlueprintCallable, Category=ThirdPersonCharacter)
		bool IsInMovement() const;

	/** Check if the character is shooting */
	UFUNCTION(BlueprintCallable, Category=ThirdPersonCharacter)
		bool IsShooting() const;

	/** Check if the character is controller with IA */
	UFUNCTION(BlueprintCallable, Category=ThirdPersonCharacter)
		bool IsIA() const;

	/** Checkthe Health of the character */
	UFUNCTION(BlueprintCallable, Category = ThirdPersonCharacter)
		int GetHealth() const;

	/** Set the new Health of the player */
	UFUNCTION(BlueprintCallable, Category=ThirdPersonCharacter)
		void SetHealth(int newHealth);

	/** Check id the character is dead */
	UFUNCTION(BlueprintCallable, Category=ThirdPersonCharacter)
		bool IsDead();

	/** Switch between first person camera and third person camera */
	UFUNCTION(BlueprintCallable, Category=ThirdPersonCharacter)
		void SwitchCamera();

	/** Set default position */
	UFUNCTION(BlueprintCallable, Category=ThirdPersonCharacter)
		void ResetMovements();

	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return ThirdPersonCamera; }

	virtual void ObjectDamaged_Implementation(AWeaponShoot* ObjectDamaging) override;

protected:
	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/** ThirdPersonCamera camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* ThirdPersonCamera;

	/** FirstPersonCamera camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FirstPersonCamera;

	/** The State of the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		CharacterState State;

	/** The shooting Weapon */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Weapon)
		TSubclassOf<class AWeaponShoot> WeaponShoot;

	/** The health of the character */
	int Health;

	/** The health of the character */
	int MaxHealth;

	/** The health of the character */
	bool bIsThirdPersonCameraActive;

	/** Timer handle of functionalities with timeDelay */
	FTimerHandle TimerHandle;

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);
	
	/** Called for shoot */
	void Shoot();

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};

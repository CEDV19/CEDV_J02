#include "ThirdPersonCharacter.h"
#include "KA47Shoot.h"
#include "BattleGameState.h"
#include "BattleAIController.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "Components/ActorComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "ConstructorHelpers.h"

//////////////////////////////////////////////////////////////////////////
// AThirdPersonCharacter

AThirdPersonCharacter::AThirdPersonCharacter() : MaxHealth(100), bIsThirdPersonCameraActive(true)
{
	// Assign class variables
	Health = MaxHealth;

	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create the third person camera camera
	ThirdPersonCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("ThirdPersonCamera"));
	ThirdPersonCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	ThirdPersonCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create the first person camera camera
	FirstPersonCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCamera->SetupAttachment(GetMesh(), FName("FirstPersonCamera"));
	FirstPersonCamera->AddLocalRotation(FRotator(0, 90, 270));
	FirstPersonCamera->bUsePawnControlRotation = true; // Camera does not rotate relative to arm
	FirstPersonCamera->SetActive(false);

	// Set the IA controller
	AIControllerClass = ABattleAIController::StaticClass();
	SetState(CharacterState::IA); // IA by default
}

//////////////////////////////////////////////////////////////////////////
// Input

void AThirdPersonCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AThirdPersonCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AThirdPersonCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	//PlayerInputComponent->BindAxis("TurnRate", this, &AThirdPersonCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	//PlayerInputComponent->BindAxis("LookUpRate", this, &AThirdPersonCharacter::LookUpAtRate);
	
	// Actions
	PlayerInputComponent->BindAction("Shoot", IE_Pressed, this, &AThirdPersonCharacter::Shoot);
}

CharacterState AThirdPersonCharacter::GetState() const
{
	return State;
}

void AThirdPersonCharacter::SetState(CharacterState NewState)
{
	State = NewState;
}

bool AThirdPersonCharacter::IsInMovement() const
{
	return State == CharacterState::Movement;
}

bool AThirdPersonCharacter::IsShooting() const
{
	return State == CharacterState::AimShoot;
}

bool AThirdPersonCharacter::IsIA() const
{
	return State == CharacterState::IA;
}

int AThirdPersonCharacter::GetHealth() const
{
	return Health;
}

void AThirdPersonCharacter::SetHealth(int newHealth)
{
	Health = FMath::Clamp(newHealth, 0, MaxHealth);
	if (Health == 0) {
		UE_LOG(LogTemp, Warning, TEXT("Im DEAD!"));
	}
}

bool AThirdPersonCharacter::IsDead()
{
	return Health == 0;
}

void AThirdPersonCharacter::SwitchCamera()
{

	if (bIsThirdPersonCameraActive) {
		ThirdPersonCamera->SetActive(false);
		FirstPersonCamera->SetActive(true);
		bUseControllerRotationYaw = true;
		bUseControllerRotationPitch = true;
	}
	else {
		FirstPersonCamera->SetActive(false);
		ThirdPersonCamera->SetActive(true);
		bUseControllerRotationYaw = false;
		bUseControllerRotationPitch = false;
		SetActorRotation(FRotator::ZeroRotator);
	}
	bIsThirdPersonCameraActive = !bIsThirdPersonCameraActive;
}

void AThirdPersonCharacter::ResetMovements()
{
	State = CharacterState::IA;
	bIsThirdPersonCameraActive = true;
}

void AThirdPersonCharacter::ObjectDamaged_Implementation(AWeaponShoot* ObjectDamaging)
{
	SetHealth(Health - ObjectDamaging->Damage);
}

void AThirdPersonCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AThirdPersonCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AThirdPersonCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f) && State == CharacterState::Movement)
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AThirdPersonCharacter::MoveRight(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f) && State == CharacterState::Movement)
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AThirdPersonCharacter::Shoot()
{
	// Set the firsperson view
	if (State == CharacterState::Movement) 
	{
		State = CharacterState::AimShoot;
		SwitchCamera();
	}
	// Shooting
	else
	{
		FVector spawnLocation = FirstPersonCamera->GetComponentLocation();
		spawnLocation.Z = spawnLocation.Z - 8;
		spawnLocation += FirstPersonCamera->GetForwardVector() * 100;
		auto Weapon = AWeaponShoot::CreateAndSpawnKA47Shoot(GetWorld(), spawnLocation, FirstPersonCamera->GetComponentRotation());
		Weapon->Shoot();
		SwitchCamera();
		ABattleGameState* BattleGameState = Cast<ABattleGameState>(GetWorld()->GetGameState());
		if (BattleGameState->TimeOfTurn > 1)
		{
			GetWorldTimerManager().SetTimer(TimerHandle, [this]() { Cast<ABattleGameState>(GetWorld()->GetGameState())->FinishTurn(); }, 1, false);
		}
	}
}

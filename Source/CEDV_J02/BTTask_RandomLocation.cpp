#include "BTTask_RandomLocation.h"
#include "BattleAIController.h"
#include "NavigationSystem.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UBTTask_RandomLocation::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ABattleAIController* IAController = Cast<ABattleAIController>(OwnerComp.GetAIOwner());
	FVector CharacterLocation = IAController->GetOwner()->GetActorLocation();
	FNavLocation NavLocation;
	UNavigationSystemV1::GetCurrent(GetWorld())->GetRandomReachablePointInRadius(CharacterLocation, 1000, NavLocation);
	IAController->GetBlackboardComponent()->SetValueAsVector(BlackboardKey.SelectedKeyName, NavLocation.Location);
	return EBTNodeResult::Succeeded;
}


#include "BTTask_CleanTarget.h"
#include "BattleAIController.h"

EBTNodeResult::Type UBTTask_CleanTarget::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ABattleAIController* IAController = Cast<ABattleAIController>(OwnerComp.GetAIOwner());
	IAController->ClearFocus(EAIFocusPriority::Default);
	return EBTNodeResult::Succeeded;
}


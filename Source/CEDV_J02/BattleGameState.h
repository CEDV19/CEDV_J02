#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameStateBase.h"
#include "Runtime/Engine/Public/TimerManager.h"
#include "ThirdPersonCharacter.h"
#include "BattleGameState.generated.h"

/** Represents wich player has the turn */
UENUM(BlueprintType)
enum class Turn : uint8
{
	Player	UMETA(DisplayName="Player"),
	IA		UMETA(DisplayName="IA")
};

/**
 * The Game State of the Battle level. Has all logic of the game
 */
UCLASS()
class CEDV_J02_API ABattleGameState : public AGameStateBase
{
	GENERATED_BODY()
	
public:
	ABattleGameState();

	/** The State of the Battle */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = BattleGameState)
		Turn GameTurn;

	/** The current time to finish the turn */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = BattleGameState)
		int TimeOfTurn;

	/** The Score */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = BattleGameState)
		int Score;

	/** Finish the turn of the player or IA */
	UFUNCTION(BlueprintCallable, Category = BattleGameState)
		void FinishTurn();

protected:
	/** The maximum of time in each turn*/
	int MaxTimeOfTurn;

	/** Timer handle of functionalities with time */
	FTimerHandle TimerTurnHandle;

	/** The Players Target Class */
	UPROPERTY()
	TSubclassOf<class ATargetPoint> PlayerTargetClass;

	/** The Enemies Target Class */
	UPROPERTY()
	TSubclassOf<class ATargetPoint> EnemyTargetClass;

	/** The Players Character Class */
	UPROPERTY()
	TSubclassOf<class AThirdPersonCharacter> PlayerCharacterClass;

	/** The Enemies Character Class */
	UPROPERTY()
	TSubclassOf<class AThirdPersonCharacter> EnemyCharacterClass;

	/** The Players units */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = BattleGameState)
	TArray<AThirdPersonCharacter*> Players;

	/** The Enemies units */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = BattleGameState)
	TArray<AThirdPersonCharacter*> Enemies;
	
	/** The Player Controller */
	UPROPERTY()
		APlayerController* Controller;

	/** The current Player */
	int CurrentPlayer;

	/** The current Player */
	int CurrentIA;

	/** Important method wich has the turn funtionalities */
	void ProcessTurn();

	/** Check win of lose conditions to finish the battle */
	void CheckFinishConditions();

	/** Remove death characters referencies */
	void RemoveDeadCharacters();

	/** Called when the game starts or when spawned */
	virtual void BeginPlay() override;
};

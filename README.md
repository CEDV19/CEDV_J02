# Mini-game 02 - Hogs of War

The second mini-game of the course [expert in video game development](http://cedv.uclm.es/). The game is an simple adaptation of [Hogs of War](https://en.wikipedia.org/wiki/Hogs_of_War) game [Unreal Engine 4](https://www.unrealengine.com).

There is available a [video of the project](https://www.dropbox.com/s/7ofldgln5rozmrq/Minijuego%202%20-%20Hogs%20of%20War.mp4?dl=0).

## Build

Hogs of War is developed using Unreal Engine 4.22 version. Hence, If you want to open, contribute or use it in your projects, you only need to open the project using this version.

## Pictures

![](https://i.imgur.com/SoK7zbi.png)

## License

Battleship is provided under [GNU General Public License Version 3](https://gitlab.com/CEDV19/CEDV_J02/blob/master/LICENSE).